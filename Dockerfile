FROM debian:10
 
RUN apt update -q -y
RUN apt install -yf \
build-essential libpoppler-cpp-dev pkg-config python3-dev \
python3 \
python3-pip

COPY ./named_entities Fil_Rouge_PdfInfo_Extraction/
WORKDIR /Fil_Rouge_PdfInfo_Extraction/

RUN pip3 install --upgrade setuptools
RUN pip3 install --upgrade pip
RUN pip install -r requirements.txt
RUN python3 -m spacy download en_core_web_sm

EXPOSE 5000
 
ENTRYPOINT FLASK_APP=extract_named_entities.py flask run --host=0.0.0.0
