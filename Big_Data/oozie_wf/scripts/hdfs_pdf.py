from hdfs.ext.kerberos import KerberosClient

client = KerberosClient('http://hdfs-nn-1.au.adaltas.cloud:50070')

client.makedirs('education/cs_2022_spring_1/k.youssef-cs/Fil_Rouge_Project/CSV_File')

client.makedirs('education/cs_2022_spring_1/k.youssef-cs/Fil_Rouge_Project/PDF_Files')

client.upload('education/cs_2022_spring_1/k.youssef-cs/Fil_Rouge_Project/PDF_Files', '../../../PDF/', n_threads=0, temp_dir=None, chunk_size=65536, progress=None, cleanup=True, overwrite = True)

client.upload('education/cs_2022_spring_1/k.youssef-cs/Fil_Rouge_Project/CSV_File', '../../../PDF_Metadata.csv', n_threads=0, temp_dir=None, chunk_size=65536, progress=None, cleanup=True, overwrite = True)

for d in client.list('education/cs_2022_spring_1/k.youssef-cs/Fil_Rouge_Project/PDF_Files'):
    print(d)
