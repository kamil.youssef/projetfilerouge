CREATE EXTERNAL TABLE IF NOT EXISTS cs_2022_spring_1.sandy_fadel_pdf_data (
  Title STRING,
  Authors STRING,
  Creation_Date Date,
  Modified_Date Date,
  Num_Pages STRING
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
LOCATION 'education/cs_2022_spring_1/s.fadel-cs/Fil_Rouge_Project/CSV_File/'
TBLPROPERTIES ('skip.header.line.count'='1');
