# Fil Rouge Project

Projects are installed on virtuel machine ubuntu (Version 20.04)

Copy the solution to your machine by using the command below:

```bash
git clone https://gitlab-student.centralesupelec.fr/kamil.youssef/projetfilerouge.git
```

## Jupyter-lab installation

To install jupyter notebook on ubuntu as **Root user**

```bash
sudo apt-get update
sudo apt install python3-pip
pip3 install jupyter
```

Then we need to install some dependencies for the pdftotext

```bash
sudo apt install build-essential libpoppler-cpp-dev pkg-config python3-dev
```



## Docker installation

Install docker

```bash
sudo apt install docker.io
```

Then run the following to create the docker container

```bash
sudo docker build -t projetfilerouge .
sudo docker run -d -p 8000:5000 projetfilerouge
```



## Run Notebook Code

Connect to Jupyter Lab and go to the projetfilerouge directory

Open the fileRouge.ipynb and run the code

Go to owl/ directory and download the onto.owl file then you can open it in Protege

## Big Data

Create the file /etc/krb5.conf :

```bash
[libdefaults]
 default_ccache_name = FILE:/tmp/krb5cc_%{uid}
 default_realm = AU.ADALTAS.CLOUD
 dns_lookup_realm = false
 dns_lookup_kdc = true
 rdns = false
 ticket_lifetime = 24h
 forwardable = true
 udp_preference_limit = 0

[realms]
 AU.ADALTAS.CLOUD = {
  kdc = ipa1.au.adaltas.cloud:88
  master_kdc = ipa1.au.adaltas.cloud:88
  admin_server = ipa1.au.adaltas.cloud:749
  default_domain = au.adaltas.cloud
 }

[domain_realm]
 .au.adaltas.cloud = AU.ADALTAS.CLOUD
 au.adaltas.cloud = AU.ADALTAS.CLOUD
 ipa1.au.adaltas.cloud = AU.ADALTAS.CLOUD
```



Install the following packages on your machine

```bash
sudo apt-get install gcc python-dev libkrb5-dev
sudo apt install krb5-user
```



Run the following command to take a ticket:

```bash
kinit $USER
```

Go to Big Data folder

To move the PDF/ file to HDFS plz go to Big_Data/oozie_wf/scripts/hdfs_pdf.py and install the requirements then run the python code

```bash
cd Big_Data/oozie_wf/scripts/
pip3 install -r requirements.txt
python3 hdfs_pdf.py
```

For the second question (Hive)  connect to zeppelin. you already have access to my folder "kamil_youssef". You can view the create table query as well as the dashboards.

For the oozie Part:

Copy the file oozie_wf to the hadoop cluster

then run the following to execute the job

```bash
kinit $USER
hdfs dfs -put -f oozie_wf/ "/user/$USER"
oozie job -run -config oozie_wf/job.properties -oozie http://oozie-1.au.adaltas.cloud:11000/oozie
```



you will have a job ID then execute the command to see the status of the job

```bash
oozie job -info 0002230-210916172301405-oozie-oozi-W -oozie http://oozie-1.au.adaltas.cloud:11000/oozie
```

